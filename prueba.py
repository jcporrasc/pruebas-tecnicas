'''
Este archivo contiene todas las paqueterías y funciones necesarias que se utilizaron
 para la creación e implementación de la
 prueba técnica solicitada el día 12/03/2021.
 Para la correcta ejecucuion del archivo, por favor leer Archivo README.txt

'''

import hashlib
import time
import requests
import pandas as pd
import numpy as np
from restcountries import RestCountryApiV2 as rapi
from sqlalchemy import create_engine

def regiones(url,headers):
    """
    Descripcion: Esta funcion hace un llamado a la API solicitada "REST Countries v1"
        Para procesar la informacion dada despues del llamado y con esto devolver
        la informacion organizada para su procesamiento

    Args:
        url: (str) Direccion "https://restcountries-v1.p.rapidapi.com/all" a la cual
                se hace el llamado correspondientes
        headers: (dict) Elemento compuesto por 2 variables correspondientes a
                    'x-rapidapi-key' credencial y 'x-rapidapi-host' host de la API

    return:
        regio: (dict) Diccionario compuesto por regiones y paises de cada region.

    """
    response = requests.request("GET", url, headers=headers)
    lista = response.json()
    regio={}
    for elem in lista:
        if elem["region"] in regio:
            pais =  regio[elem["region"]]
            pais.append(elem["name"])
        else:
            regio[elem["region"]]=[elem["name"]]
    return regio

def country(region_country):
    """
    Descripcion: Funcion que organiza en una lista los datos dandon como retorno la region,
                    el pais de la region (aleatorio) y los idiomas que hablan en el pais
                    por medio de la api que solicitan en el punto 2 y 3 de la prueba como se
                    solicito, aun que se podian obtener estos datos del llamado del punto 1
                    lo cual hubiera facilitado el diseño de la respuesta de la prueba.
    Args:
        region_country: (dict) diccionario de regiones y sus paises

    return:
        r_and_c: (list) lista donde cada elemento de r_c_l es una lista compuesta por
                    [region , pais, [{caracteristicas de lenguaje}]]
                    este ultimo elemento se asigna como una lista ya que hay paises con varios
                    lenguajes.

    """
    region = list(region_country.keys())
    r_and_c =[]
    for i in region:
        if i == "":
            pais= region_country[i][np.random.randint(len(region_country[i]))]
            country_list = rapi.get_countries_by_name(pais)
            countrys = country_list[0]
            r_and_c.append([i,
                            region_country[i][np.random.randint(len(region_country[i]))],
                            countrys.languages])

        else:
            country_list = rapi.get_countries_by_region(i)
            rand= np.random.randint(len(country_list))
            countrys = country_list[rand]
            r_and_c.append([i,countrys.name,countrys.languages])
    return r_and_c

def r_and_c_and_l(diccionario):
    """
    Descripcion: Recibe el diccionario de regiones y paises para lograr seleccionar y
                     dar un orden optimo a los datos para trabajar de una buena manera con ellos.
    Args:
        diccionario: (dict) diccionario de regiones y sus paises.

    return:
        r_c_l: (list) lista con datos bien organizados como listas, compuestas de la manera
                     [region, pais [lenguajes]] donde lenguajes es uno o mas str de los nombres
                     de las lenguas que se hablan en ese pais.

    """
    r_c_l=[]
    r =""
    reg_and_coun =country(diccionario)
    for i in range(len(reg_and_coun)):
        r = reg_and_coun[i][0]
        c = reg_and_coun[i][1]
        l =[]
        for x in range(len(reg_and_coun[i][-1])):
            l.append(reg_and_coun[i][-1][x]["name"])
        r_c_l.append([r,c,l])
    return r_c_l


def encrip(info):
    """
    Descripcion: Recibe una lista cuyo ultimo elemento es una lista de strs y se
                    encarga de encriptarlos en SHA1, devolviendo los datos final de la prueba.
    Args:
        info: (list) lista compuesta por elementos [region, pais [lenguajes]]

    return:
        filas: (list) lista que como su nombre dice, son las filas del DataFrame en su totalidad
                        por lo cual se calcula el tiempo de creacion de las filas en esta funcion
                        de esta manera el elemento filas se compone por
                        [region, pais, [idiomas],[idiomas encriptados],tiempo]
                        donde tiempo es el tiempo que dura en generar este elemento.

    """
    filas=[]
    for i in range(len(info)):
        inicio = time.time()
        leng=[]
        leng_enc=[]
        for x in info[i][-1]:
            leng.append(x)
            h = hashlib.sha1()
            h.update(x.encode('utf-8'))
            leng_enc.append(h.hexdigest())
        fin = time.time()
        filas.append([info[i][0],info[i][1],leng, leng_enc,(fin-inicio)*1000])
    return filas


def new_df(data):
    """
    Descripcion: Crea un dataframe caracteristico con la informacion que se necesita
                    para resolver la prueba, la columna tiempo esta dada en milisegundos.
    Args:
        data: (list) lista donde cada elemento es una fila del dataframe final quitando
                        el idioma sin encriptar

    return:
        df: (pandas.Dataframe) dataframe generado por paqueteria pandas como se solicita en
                        la prueba.

    """
    df = pd.DataFrame(columns = ['Region' , 'City Name', 'Languaje','Time(ms)'])
    for i in range(len(data)):
        df = df.append({
        'Region': data[i][0],
        'City Name':data[i][1],
        'Languaje':data[i][3],
        'Time(ms)':data[i][4]}, ignore_index=True)
    return df


def times(times_col):
    """
    Descripcion: Calcula por medio de la paqueteria pandas datos respecto a los tiempos
                    por celda.
    Args:
        times_col: (columna de pandas.Df) columna de valores de tiempos de creacion

    return:
        (None): no devuelve nada, ya que el punto especifica que debe imprimir y guardar la
                    informacion generada en un sqlite, en este cada se guarda en "Tiempos.sqlite"

    """
    t= pd.DataFrame(columns=['Tiempo total' , 'Tiempo Promedio', 'Tiempo minimo','Tiempo maximo'])
    t= t.append({'Tiempo total':times_col.sum() ,
               'Tiempo Promedio':times_col.mean(),
               'Tiempo minimo':times_col.min(),
               'Tiempo maximo':times_col.max()}, ignore_index=True)
    print(t)
    engine = create_engine("sqlite://", echo=False)
    t.to_sql("Tiempos.sqlite",con=engine)
    
