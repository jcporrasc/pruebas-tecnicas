# Pruebas Tecnicas

Proyecto escrito en Python 3.X para la creacion de una tabla que de por region un pais, sus idiomas encriptados en SHA1 y el tiempo que duro en crear esta fila.

## Modo de uso:
Su uso es supremamente sencillo, simplemente ingrese a  https://rapidapi.com/apilayernet/api/rest-countries-v1 donde podra obtener credenciales individuales para el uso de la Api, igualmente al generar el codigo en esta misma direccion se crean 2 variables las cuales son:

url = "https://restcountries-v1.p.rapidapi.com/all"

headers = {
    'x-rapidapi-key': "SUS CREDENCIALES",
    'x-rapidapi-host': "restcountries-v1.p.rapidapi.com"
    }

una vez obtenidos estos elementos, correr el script es facil:
0) asegurarse de tener instalada toda la paqueteria necesaria que se encuentra descrita en el archivo requirements.txt
1) asegurarse de descargar preubas.py y main.py en un mismo lugar
2) abrir main.py y cambiar dentro de la funcion main() los valores de headers por sus credenciales 
3) ejecutar main.py 

Y listo! 
se genera un archivo .json con la informacion obtenida y una tabla con una pequeña descripcion estadistica de los tiempos de ejecucion del script
