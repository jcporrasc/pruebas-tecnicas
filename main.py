"""
Script inicializador de la prueba:
Aqui se deben poner los parametros iniciales url y headers
Se deja predispuestas credenciales del autor.

Autor:
        Joseph Camilo Porras Castiblanco.

"""
import prueba as p

def main():
    """
    Descripcion:
        Funcion inicializadora
        No se crea una funcion para guardar los datos ya que al tenerlos
        en un dataframe, por medio de una funcion propia de pandas se puede
        guardar en formato .json en una sola linea, crear una funcion no
        optimizaria nada.

    """
    url = "https://restcountries-v1.p.rapidapi.com/all"

    headers = {
    'x-rapidapi-key': "c55f4f43f8msh443f166cb1daea3p12e125jsn5a7b28149a80",
    'x-rapidapi-host': "restcountries-v1.p.rapidapi.com"
    }

    region_and_country = p.regiones(url, headers)
    data = p.r_and_c_and_l(region_and_country)
    data = p.encrip(data)
    table = p.new_df(data)
    p.times(table["Time(ms)"])
    table.to_json(r"data.json")

if __name__=="__main__":
    main()
